<?php
namespace App\Api;

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\SpreadsheetService;

class GoogleSheet
{

    public static function insert(array $contact, string $city)
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . dirname(app_path()) . '/google-sheet.json');
        /*  SEND TO GOOGLE SHEETS */
        $client = new \Google_Client;
        try{
            $client->useApplicationDefaultCredentials();
            $client->setApplicationName("Something to do with my representatives");
            $client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
            if ($client->isAccessTokenExpired()) {
                $client->refreshTokenWithAssertion();
            }
            $accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
            ServiceRequestFactory::setInstance(
                new DefaultServiceRequest($accessToken)
            );
            // Get our spreadsheet
            $spreadsheet = (new SpreadsheetService)
                ->getSpreadsheetFeed()
                ->getByTitle('Contacts');

            // Get the first worksheet (tab)
            $worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
            $worksheet = $worksheets[0];
            $listFeed = $worksheet->getListFeed();
            $listFeed->insert([
                'name' => $contact['name'],
                'telephone' => $contact['tel'],
                'email' => $contact['email'],
                'city' => $city,
                'date' => date_create('now')->format('Y-m-d H:i:s')
            ]);
        }catch(Exception $e){
            echo $e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile() . ' ' . $e->getCode;
        }
    }
}